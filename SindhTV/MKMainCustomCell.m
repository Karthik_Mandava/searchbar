//
//  MKMainCustomCell.m
//  ChaatHouse
//
//  Created by Intellisense Technology on 06/05/16.
//  Copyright © 2016 intellisense. All rights reserved.
//

#import "MKMainCustomCell.h"

@implementation MKMainCustomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
